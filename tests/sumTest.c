 

#include "sum.h"
#include "unity.h"

//sometimes you may want to get at local data in a module.
//for example: If you plan to pass by reference, this could be useful
//however, it should often be avoided
extern int Counter; 

void setUp(void)
{
  //This is run before EACH TEST
  //Counter = 0x5a5a;
}

void tearDown(void)
{
}

void test_Sum_Function(void)
{
  //All of these should pass
  TEST_ASSERT_EQUAL(5, sum(2,3));
  TEST_ASSERT_EQUAL(6, sum(3,3));
  TEST_ASSERT_EQUAL(7, sum(2,5));

}

void test_Extract_Function(void)
{
  
  TEST_ASSERT_EQUAL(0, extract(3,3));
  
}

void test_message(void)
{
  int a = 3;
  TEST_ASSERT_MESSAGE( a == 2 , "a isn't 2, end of the world!");
}